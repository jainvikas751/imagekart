package com.android.imagekart.android.domain

object Constants {
    const val X_AUTH_KEY: String = "Authorization"
    const val X_AUTH_VALUE: String = "Client-ID 137cda6b5008a7c"
    const val BASE_URL: String = "https://api.imgur.com/3/"
    const val API_ERROR_RESPONSE_KEY: String = "error"

    const val HIDE_PROGRESS: String = "hide_progress"
    const val SHOW_PROGRESS: String = "show_progress"
    const val ARG_IMAGE: String = "arg_image"
    const val SHOW_DIALOG: String = "show_dialog"
}