package com.android.imagekart.android.data.di.module

import com.android.imagekart.android.data.di.scope.ActivityScope
import com.android.imagekart.android.view.activity.imagedetails.ImageDetailsActivity
import com.android.imagekart.android.view.activity.imagesearchscreen.ImageSearchActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {
    @ActivityScope
    @ContributesAndroidInjector
    abstract fun bindImageSearchActivity(): ImageSearchActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun bindImageDetailsActivity(): ImageDetailsActivity
}