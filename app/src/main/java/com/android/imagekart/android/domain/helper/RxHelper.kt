package com.android.imagekart.android.domain.helper

import com.android.imagekart.android.data.DataSource
import com.android.imagekart.android.data.model.ImageCommentItem
import com.android.imagekart.android.data.model.ImageResponse
import com.android.imagekart.android.data.remote.CallbackObserverWrapper
import com.android.imagekart.android.data.room.dao.ImageCommentItemRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RxHelper @Inject constructor(
    private val dataSource: DataSource
) {

    interface RxHelperCallback<R> {
        fun onSuccess(response: R)
        fun onFailure(errorMessage: String)
    }

    fun getImages(
        query: String,
        callback: RxHelperCallback<ImageResponse>
    ) {
        dataSource.getImages(query)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : CallbackObserverWrapper<ImageResponse>() {
                override fun onSuccess(imageResponse: ImageResponse) {
                    callback.onSuccess(imageResponse)
                }

                override fun onFailure(error: String) {
                    callback.onFailure(error)
                }
            })
    }

    fun insertComment(comment : ImageCommentItem) {
        ImageCommentItemRepository.insertComment(comment)
    }

}