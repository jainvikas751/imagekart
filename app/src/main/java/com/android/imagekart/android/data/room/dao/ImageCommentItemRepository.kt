package com.android.imagekart.android.data.room.dao

import com.android.imagekart.android.data.model.ImageCommentItem
import com.android.imagekart.android.data.room.AppDatabase
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ImageCommentItemRepository {
    companion object {
        fun insertComment(comment: ImageCommentItem) {
            comment.let {
                Observable.fromCallable<Unit> {
                    return@fromCallable AppDatabase.get().imageCommentItemDao().insert(it)
                }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe()
            }
        }
    }
}