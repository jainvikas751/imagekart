package com.android.imagekart.android.data.di


import com.android.imagekart.android.ImageKartApplication
import com.android.imagekart.android.data.di.component.DaggerApplicationComponent

object ApplicationInjector {
    fun inject(imageKartApplication: ImageKartApplication) {
        DaggerApplicationComponent.builder()
            .application(imageKartApplication).build()
            .inject(imageKartApplication)
    }
}