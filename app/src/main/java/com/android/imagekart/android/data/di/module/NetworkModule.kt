package com.android.imagekart.android.data.di.module

import android.content.Context
import com.android.imagekart.android.data.DataSource
import com.android.imagekart.android.data.remote.ImageKartApi
import com.android.imagekart.android.domain.Constants
import com.android.imagekart.android.domain.helper.RxHelper
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
class NetworkModule {
    @Provides
    @Singleton
    fun okHttp3Client(context: Context): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()

        return OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor { chain ->

                val request = chain.request().newBuilder()
                    .addHeader(Constants.X_AUTH_KEY, Constants.X_AUTH_VALUE).build()
                chain.proceed(request)
            }
            .readTimeout(0, TimeUnit.MILLISECONDS)
            .connectTimeout(0, TimeUnit.MILLISECONDS)
            .build()
    }

    @Provides
    @Singleton
    fun jacksonConverterFactory(): JacksonConverterFactory {
        val objectMapper = ObjectMapper()
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        return JacksonConverterFactory.create(objectMapper)
    }

    @Provides
    @Singleton
    fun retrofit(okHttpClient: OkHttpClient, jacksonConverterFactory: JacksonConverterFactory): Retrofit {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(jacksonConverterFactory)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    @Provides
    @Singleton
    fun imageKartApi(retrofit: Retrofit): ImageKartApi {
        return retrofit.create(ImageKartApi::class.java)
    }


    @Provides
    fun provideRxHelper(dataSource: DataSource): RxHelper {
        return RxHelper(dataSource)
    }

}