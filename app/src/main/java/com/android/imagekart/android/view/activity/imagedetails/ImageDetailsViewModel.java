package com.android.imagekart.android.view.activity.imagedetails;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.android.imagekart.android.data.model.ImageCommentItem;
import com.android.imagekart.android.data.model.ImageListItem;
import com.android.imagekart.android.data.room.AppDatabase;
import com.android.imagekart.android.domain.Constants;
import com.android.imagekart.android.domain.helper.RxHelper;
import com.android.imagekart.android.view.base.BaseViewModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

// view model for image details and add comment screen
public class ImageDetailsViewModel extends BaseViewModel {
    private RxHelper rxHelper;
    private MutableLiveData<List<ImageCommentItem>> imageCommentItemList = new MutableLiveData<>();
    private MutableLiveData<ImageListItem> imageListItem = new MutableLiveData<>();
    private MutableLiveData<ImageCommentItem> newComment = new MutableLiveData<>();

    public MutableLiveData<ImageListItem> getImageListItem() {
        return imageListItem;
    }

    public MutableLiveData<List<ImageCommentItem>> getImageCommentItemList() {
        return imageCommentItemList;
    }

    public MutableLiveData<ImageCommentItem> getNewComment() {
        return newComment;
    }

    @Inject
    ImageDetailsViewModel(RxHelper rxHelper) {
        this.rxHelper = rxHelper;
    }


    // insert comment into database
    void insertComment(String comment) {
        if (comment != null && !comment.isEmpty()) {
            ImageCommentItem imageCommentItem = new ImageCommentItem(imageListItem.getValue().getId(), comment);
            newComment.setValue(imageCommentItem);
            rxHelper.insertComment(imageCommentItem);
        }
    }

    // get comments form database
    @SuppressLint("CheckResult")
    void getCommentsFromDb() {
        Observable.fromCallable(() -> AppDatabase.Companion.get().imageCommentItemDao().getAll(imageListItem.getValue().getId()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(imageCommentItems -> {
                    getProgressDialogState().setValue(Constants.HIDE_PROGRESS);
                    imageCommentItemList.setValue(imageCommentItems);
                });
    }

    // get data from intent
    public void getIntentData(Intent intent) {
        getProgressDialogState().setValue(Constants.SHOW_PROGRESS);
        imageListItem.setValue(intent.getParcelableExtra(Constants.ARG_IMAGE));
        getCommentsFromDb();
    }
}
