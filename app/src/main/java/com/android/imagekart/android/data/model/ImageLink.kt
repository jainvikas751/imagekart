package com.android.imagekart.android.data.model

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ImageLink(
    @JsonProperty("link")
    var link: String? = null
) :Parcelable