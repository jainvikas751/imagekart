package com.android.imagekart.android.view.activity.imagesearchscreen

import androidx.lifecycle.MutableLiveData
import com.android.imagekart.android.data.model.ImageListItem
import com.android.imagekart.android.data.model.ImageResponse
import com.android.imagekart.android.domain.Constants
import com.android.imagekart.android.domain.helper.RxHelper
import com.android.imagekart.android.view.base.BaseViewModel
import javax.inject.Inject


// view model for image search screen
class ImageSearchViewModel @Inject constructor(
    private val rxHelper: RxHelper
) : BaseViewModel() {

    private var imageListItemListMutableData: MutableLiveData<ArrayList<ImageListItem>> = MutableLiveData()
    private var emptyListMutableData: MutableLiveData<Boolean> = MutableLiveData()
    private var clearListMutableData: MutableLiveData<Boolean> = MutableLiveData()

    // mutable live data for image list. once list is fetched tis is assigned.
    fun getImageListMutableData() = imageListItemListMutableData

    // mutable live data for image list. once list is fetched tis is assigned.
    fun getEmptyListMutableData() = emptyListMutableData

    // mutable live data for clearing list
    fun getClearListMutableData() = clearListMutableData

    // function for fetching images over th api
    fun getImages(query: String) {
        progressDialogState.value = Constants.SHOW_PROGRESS
        emptyListMutableData.value = false
        clearListMutableData.value = true
        rxHelper.getImages(query, object : RxHelper.RxHelperCallback<ImageResponse> {
            override fun onSuccess(response: ImageResponse) {
                progressDialogState.value = Constants.HIDE_PROGRESS
                if (!response.imageListItemList.isNullOrEmpty()) {
                    emptyListMutableData.value = false
                    imageListItemListMutableData.value = response.imageListItemList
                } else {
                    emptyListMutableData.value = true
                }
            }

            override fun onFailure(errorMessage: String) {
                progressDialogState.value = Constants.HIDE_PROGRESS
                apiErrors.value = errorMessage
            }

        })
    }
}