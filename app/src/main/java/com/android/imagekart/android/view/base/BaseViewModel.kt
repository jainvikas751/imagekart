package com.android.imagekart.android.view.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

//This is base view model. All other view models in app will extend this.

abstract class BaseViewModel() : ViewModel() {

    // live data for api errors
    var apiErrors: MutableLiveData<String> = MutableLiveData()
    //live data for progress dialog
    var progressDialogState: MutableLiveData<String> = MutableLiveData()

    open fun onApiErrors() {

    }

}