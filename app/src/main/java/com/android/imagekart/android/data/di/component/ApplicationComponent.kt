package com.android.imagekart.android.data.di.component

import android.app.Application
import com.android.imagekart.android.ImageKartApplication
import com.android.imagekart.android.data.di.module.ActivityBuilderModule
import com.android.imagekart.android.data.di.module.ApplicationModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidInjectionModule::class, AndroidSupportInjectionModule::class,
        ApplicationModule::class, ActivityBuilderModule::class]
)
interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

    fun inject(imageKartApplication: ImageKartApplication)
}