package com.android.imagekart.android.domain.helper

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions

object GlideUtil {

    //for displaying image
    fun showImage(imageView: ImageView, imgUrl: String, placeHolder: Int) {
        Glide.with(imageView.context)
            .load(imgUrl)
            .apply(getRequestOptions(placeHolder)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(false))
            .into(imageView)
    }

    private fun getRequestOptions(placeHolder: Int): RequestOptions {
        return RequestOptions()
            .placeholder(placeHolder)
            .error(placeHolder)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .fitCenter()
    }
}