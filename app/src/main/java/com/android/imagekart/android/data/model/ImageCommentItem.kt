package com.android.imagekart.android.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ImageCommentItem(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,

    var imageId: String? = null,

    var comment: String? = null
) {
    constructor(imageId: String, comment: String) : this(0, imageId, comment)
}