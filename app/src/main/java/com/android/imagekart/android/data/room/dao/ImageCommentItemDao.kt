package com.android.imagekart.android.data.room.dao

import androidx.room.*
import com.android.imagekart.android.data.model.ImageCommentItem
import com.android.imagekart.android.data.model.ImageListItem

@Dao
interface ImageCommentItemDao {
    @Query("SELECT * FROM imagecommentitem WHERE imageId = :imageId")
    fun getAll(vararg imageId: String): List<ImageCommentItem>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(vararg imagecommentitem: ImageCommentItem)

    @Delete
    fun delete(labelListItem: ImageCommentItem)

    @Query("DELETE FROM imagecommentitem")
    fun deleteAll()

    @Query("DELETE FROM imagecommentitem WHERE id = :id")
    fun deleteItemById(id: Int?)
}