package com.android.imagekart.android.data.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.android.imagekart.android.data.model.ImageCommentItem
import com.android.imagekart.android.data.room.dao.ImageCommentItemDao


@Database(
    entities = [ImageCommentItem::class],
    version = 1,
    exportSchema = false
)

abstract class AppDatabase : RoomDatabase() {
    abstract fun imageCommentItemDao(): ImageCommentItemDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun get(): AppDatabase =
            INSTANCE ?: throw Exception("-> Not yet initialised")

        fun init(context: Context): AppDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: Room.databaseBuilder(context, AppDatabase::class.java, "imagekart.db")
                    .build().also { INSTANCE = it }
            }
    }
}