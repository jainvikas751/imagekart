package com.android.imagekart.android.data.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.android.imagekart.android.data.di.scope.ViewModelKey
import com.android.imagekart.android.domain.helper.ViewModelProviderFactory
import com.android.imagekart.android.view.activity.imagedetails.ImageDetailsViewModel
import com.android.imagekart.android.view.activity.imagesearchscreen.ImageSearchViewModel

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelBuilderModule {
    @Binds
    abstract fun bindViewModelProviderFactory(factory: ViewModelProviderFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ImageSearchViewModel::class)
    abstract fun bindImageViewModel(viewModel: ImageSearchViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ImageDetailsViewModel::class)
    abstract fun bindImageDetailsViewModel(viewModel: ImageDetailsViewModel): ViewModel
}