package com.android.imagekart.android.view.activity.imagedetails;


import android.widget.Toast;
import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.android.imagekart.R;
import com.android.imagekart.android.data.model.ImageCommentItem;
import com.android.imagekart.android.domain.helper.GlideUtil;
import com.android.imagekart.android.view.base.BaseActivity;
import com.android.imagekart.databinding.ActivityImageDetailsBinding;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import javax.inject.Inject;
import dagger.android.AndroidInjection;

//This is activity for image search screen
public class ImageDetailsActivity extends BaseActivity<ActivityImageDetailsBinding, ImageDetailsViewModel> {

    @Inject
    ViewModelProvider.Factory factory;

    ActivityImageDetailsBinding activityImageDetailsBinding;

    // adapter for comments for the image
    @Inject
    ImageCommentAdapter imageCommentAdapter;

    @Override
    protected void injectDependency() {
        AndroidInjection.inject(this);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_image_details;
    }

    @NotNull
    @Override
    protected ImageDetailsViewModel getViewModel() {
        return ViewModelProviders.of(this, factory).get(ImageDetailsViewModel.class);
    }

    @Override
    protected int getVariableId() {
        return BR.viewModel;
    }

    @Override
    protected void setupBindingAndAttachView() {
        activityImageDetailsBinding = viewDataBinding();
    }

    // function for initialization.
    @Override
    protected void initView() {
        activityImageDetailsBinding.toolbar.imgArrowBack.setOnClickListener(view -> finish());

        // adapter init
        activityImageDetailsBinding.rvComments.setAdapter(imageCommentAdapter);
        activityImageDetailsBinding.rvComments.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL,
                false));

        // handles submit button onclick listener
        activityImageDetailsBinding.btnSubmit.setOnClickListener(view -> {
            hideKeyboard();
            getViewModel().insertComment(activityImageDetailsBinding.etComment.getText().toString());
        });

        // displaying selected image details
        getViewModel().getImageListItem().observe(this, imageListItem -> {
            GlideUtil.INSTANCE.showImage(activityImageDetailsBinding.imageView, imageListItem.getImageLink().get(0).getLink(), R.drawable.ic_image_placholder);
            activityImageDetailsBinding.toolbar.txtCenter.setText(imageListItem.getTitle());
        });

        // observes live data for displaying comments
        getViewModel().getImageCommentItemList().observe(this,
                imageCommentItems ->
                        imageCommentAdapter.setCommentList((ArrayList<ImageCommentItem>) imageCommentItems));

        // observes live data when user adds new comment
        getViewModel().getNewComment().observe(this, imageCommentItem -> {
            activityImageDetailsBinding.etComment.setText(null);
            Toast.makeText(this, "Comment added successfully!", Toast.LENGTH_SHORT).show();
            imageCommentAdapter.addComment(imageCommentItem);
        });

        // get selected image data from intent
        getViewModel().getIntentData(getIntent());
    }
}