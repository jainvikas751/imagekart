package com.android.imagekart.android.view.base

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import com.android.imagekart.R
import com.android.imagekart.android.domain.Constants
import com.android.imagekart.android.domain.helper.DialogUtil


// base activity for all the activities used in app
abstract class BaseActivity<VDB : ViewDataBinding, BVM : BaseViewModel> : AppCompatActivity() {
    private lateinit var viewDataBinding: VDB
    private var baseViewModel: BVM? = null
    private var dialog: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        injectDependency()
        super.onCreate(savedInstanceState)
        initViewDataBinding()
        progressDialogState()
        initApiErrors()
        setupBindingAndAttachView()
        initView()
    }


    // observe mutable data for showing progress dialog
    private fun progressDialogState() {
        baseViewModel?.progressDialogState?.observe(this, Observer {
            when (it) {
                Constants.SHOW_PROGRESS -> {
                    if (dialog == null) dialog = Dialog(this, R.style.AppTheme_FullScreenProgressBar)
                    dialog?.let { d -> if (!d.isShowing) DialogUtil.showPleaseWaitDialog(d) }
                }
                Constants.HIDE_PROGRESS -> {
                    if (dialog != null && dialog?.isShowing == true) {
                        DialogUtil.hideDialog(dialog)
                    }
                }
            }
        })
    }

    // observe mutable live data for api errors
    private fun initApiErrors() {
        baseViewModel?.apiErrors?.observe(this, Observer {
            DialogUtil.showErrorDialog(this, layoutInflater, 0, it ?: "") {
                baseViewModel?.onApiErrors()
            }
        })

    }

    // init data binding for view
    private fun initViewDataBinding() {
        viewDataBinding = DataBindingUtil.setContentView(this, layoutResource)
        baseViewModel = baseViewModel?.let { baseViewModel } ?: viewModel
        (viewDataBinding as? ViewDataBinding)?.setVariable(variableId, baseViewModel)
        (viewDataBinding as? ViewDataBinding)?.executePendingBindings()
    }

    // to hide keyboard
    fun hideKeyboard() {
        val view = currentFocus
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view?.windowToken, 0)
    }

    fun viewDataBinding(): VDB = viewDataBinding
    // below all are abstract functions or variables which are implemented by activities
    protected abstract fun injectDependency()

    protected abstract val layoutResource: Int

    protected abstract val viewModel: BVM

    protected abstract val variableId: Int

    protected abstract fun setupBindingAndAttachView()

    protected abstract fun initView()
}