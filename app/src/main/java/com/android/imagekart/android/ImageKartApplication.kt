package com.android.imagekart.android

import android.app.Activity
import android.app.Service
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.android.imagekart.android.data.di.ApplicationInjector
import com.android.imagekart.android.data.room.AppDatabase
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.HasServiceInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class ImageKartApplication : MultiDexApplication(), HasActivityInjector, HasSupportFragmentInjector, HasServiceInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var serviceinjector: DispatchingAndroidInjector<Service>

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<androidx.fragment.app.Fragment>

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)
        ApplicationInjector.inject(this)
        AppDatabase.init(applicationContext)
    }

    override fun activityInjector(): AndroidInjector<Activity> = androidInjector

    override fun serviceInjector(): AndroidInjector<Service> {
        return serviceinjector
    }

    override fun supportFragmentInjector(): AndroidInjector<androidx.fragment.app.Fragment> {
        return fragmentInjector
    }
}