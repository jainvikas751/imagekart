package com.android.imagekart.android.domain.helper

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import androidx.databinding.DataBindingUtil
import com.android.imagekart.R
import com.android.imagekart.databinding.LayoutCustomDailogBinding

class DialogUtil {

    companion object {
        fun showPleaseWaitDialog(dialog: Dialog) {
            dialog.setContentView(R.layout.layout_progress_dialog)
            dialog.setCancelable(false)
            dialog.show()
        }

        fun hideDialog(dialog: Dialog?) {
            if (dialog != null && dialog.isShowing) dialog.dismiss()
        }

        fun showErrorDialog(
            context: Context,
            layoutInflater: LayoutInflater,
            errorTitle: Int,
            message: String,
            okClickListener: ((v: View) -> Unit)? = null
        ) {
            val dialog = Dialog(context, R.style.Theme_AppCompat_Light_Dialog_Alert)
            val dialogBinding = DataBindingUtil.inflate<LayoutCustomDailogBinding>(
                layoutInflater,
                R.layout.layout_custom_dailog,
                null,
                false
            )
            dialog.setContentView(dialogBinding.root)
            dialog.setCanceledOnTouchOutside(false)
            dialogBinding.txtDialogMessage.text = message
            dialogBinding.btnDailogOk.setOnClickListener {
                dialog.dismiss()
                okClickListener?.invoke(it)
            }

            dialog.show()
        }
    }
}