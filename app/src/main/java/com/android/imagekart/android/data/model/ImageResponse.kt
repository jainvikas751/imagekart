package com.android.imagekart.android.data.model

import com.fasterxml.jackson.annotation.JsonProperty

data class ImageResponse(
    @JsonProperty("success")
    var success: Boolean = false,
    @JsonProperty("data")
    var imageListItemList: ArrayList<ImageListItem>? = null
)