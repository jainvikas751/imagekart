package com.android.imagekart.android.data.model

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ImageListItem(
    @JsonProperty("id")
    var id: String? = null,

    @JsonProperty("title")
    var title: String? = null,

    @JsonProperty("images")
    var imageLink: ArrayList<ImageLink>? = null
) : Parcelable