package com.android.imagekart.android.view.activity.imagedetails;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.imagekart.android.data.model.ImageCommentItem;

import java.util.ArrayList;

import javax.inject.Inject;

public class ImageCommentAdapter extends RecyclerView.Adapter<ImageCommentAdapter.ImageCommentViewHolder> {
    private ArrayList<ImageCommentItem> commentList = new ArrayList<>();

    @Inject
    ImageCommentAdapter() {
    }

    public void setCommentList(ArrayList<ImageCommentItem> commentList) {
        this.commentList = commentList;
        notifyDataSetChanged();
    }

    public void addComment(ImageCommentItem imageCommentItem) {
        this.commentList.add(0, imageCommentItem);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ImageCommentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);

        return new ImageCommentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageCommentViewHolder holder, int position) {
        final ImageCommentItem comment = commentList.get(position);
        holder.textView.setText(comment.getComment());
    }

    @Override
    public int getItemCount() {
        return commentList.size();
    }

    class ImageCommentViewHolder extends RecyclerView.ViewHolder {
        TextView textView = itemView.findViewById(android.R.id.text1);

        public ImageCommentViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
