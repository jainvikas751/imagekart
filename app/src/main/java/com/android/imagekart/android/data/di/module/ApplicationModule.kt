package com.android.imagekart.android.data.di.module

import android.app.Application
import android.content.Context
import com.android.imagekart.android.data.di.module.ViewModelBuilderModule
import dagger.Binds
import dagger.Module

@Module(includes = [NetworkModule::class, ViewModelBuilderModule::class])
abstract class ApplicationModule {
    @Binds
    abstract fun bindApplicationContext(application: Application): Context

}