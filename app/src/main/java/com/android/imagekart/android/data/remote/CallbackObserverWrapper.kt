package com.android.imagekart.android.data.remote

import com.android.imagekart.android.domain.Constants
import io.reactivex.observers.DisposableObserver
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException

abstract class CallbackObserverWrapper<R> : DisposableObserver<R>() {

    protected abstract fun onSuccess(result: R)

    protected abstract fun onFailure(error: String)

    override fun onNext(result: R) {
        onSuccess(result)
    }

    override fun onError(e: Throwable) {
        e.printStackTrace()
        if (e is HttpException) {
            val responseBody = e.response().errorBody()
            onFailure(getErrorMessage(responseBody))
        } else if (e is SocketTimeoutException) {
            onFailure("Network Timeout")
        } else if (e is IOException) {
            onFailure("Failed to connect to server")
        } else
            onFailure(e.localizedMessage)
    }

    override fun onComplete() {}

    private fun getErrorMessage(responseBody: ResponseBody?): String {
        var responseString = ""
        try {
            responseString = responseBody!!.string()
            val jsonObject = JSONObject(responseString)
            return jsonObject.getString(Constants.API_ERROR_RESPONSE_KEY)
        } catch (e: Exception) {
            return responseString
        }

    }
}