package com.android.imagekart.android.view.activity.imagesearchscreen

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import com.android.imagekart.R
import com.android.imagekart.android.data.model.ImageListItem
import com.android.imagekart.android.domain.helper.GlideUtil
import javax.inject.Inject


class ImageAdapter @Inject constructor(private val context: Context) : BaseAdapter() {
    private lateinit var viewModel: ImageSearchViewModel
    private var imageListItemList: ArrayList<ImageListItem> = ArrayList()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val imageView: ImageView
        val inflater =
            parent?.context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view: View = inflater.inflate(R.layout.row_image_item, null)

        imageView = view.findViewById(R.id.img_view)
        imageListItemList[position].imageLink?.get(0)?.link?.let {
            GlideUtil.showImage(
                imageView,
                it, R.drawable.ic_image_placholder
            )
        }
        return view
    }

    override fun getItem(position: Int): ImageListItem {
        return imageListItemList[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return imageListItemList.size
    }

    fun setImageList(imagesList: ArrayList<ImageListItem>) {
        this.imageListItemList.clear()
        this.imageListItemList = imagesList
    }

    fun setViewModel(viewModel: ImageSearchViewModel) {
        this.viewModel = viewModel
    }

    fun clearListData() {
        this.imageListItemList.clear()
    }
}