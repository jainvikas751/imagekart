package com.android.imagekart.android.data

import android.content.Context
import com.android.imagekart.android.data.model.ImageResponse
import com.android.imagekart.android.data.remote.ImageKartApi
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataSource @Inject constructor(
    private val context: Context,
    private val imageKartApi: ImageKartApi
) {

    fun getImages(query : String) : Observable<ImageResponse> {
        return imageKartApi.getImages(query)
    }
}