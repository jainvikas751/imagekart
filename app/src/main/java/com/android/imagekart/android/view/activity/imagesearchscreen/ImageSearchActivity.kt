package com.android.imagekart.android.view.activity.imagesearchscreen

import android.content.Intent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.TextView.OnEditorActionListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.android.imagekart.BR
import com.android.imagekart.R
import com.android.imagekart.android.domain.Constants
import com.android.imagekart.android.view.activity.imagedetails.ImageDetailsActivity
import com.android.imagekart.android.view.base.BaseActivity
import com.android.imagekart.databinding.ActivityImageSearchBinding
import dagger.android.AndroidInjection
import javax.inject.Inject

//This is activity for image search screen
class ImageSearchActivity : BaseActivity<ActivityImageSearchBinding, ImageSearchViewModel>() {
    @Inject
    lateinit var factory: ViewModelProvider.Factory

    // this is adapter for displaying images for searched query.
    @Inject
    lateinit var adapter: ImageAdapter

    // data binding variable for it's layout
    private lateinit var activityImageSearchBinding: ActivityImageSearchBinding

    override fun injectDependency() {
        AndroidInjection.inject(this)
    }

    override val layoutResource: Int = R.layout.activity_image_search

    override val viewModel: ImageSearchViewModel
        get() {
            return ViewModelProviders.of(this, factory).get(ImageSearchViewModel::class.java)
        }

    override val variableId: Int = BR.viewModel

    override fun setupBindingAndAttachView() {
        activityImageSearchBinding = viewDataBinding()
    }

    // all the initialization work is done in this method. like setting adapter
    override fun initView() {
        activityImageSearchBinding.gridView.adapter = adapter
        activityImageSearchBinding.btnSearch.setOnClickListener {
            viewModel.getImages(activityImageSearchBinding.etSearch.text.toString())
        }

        // observes live data for imagelist which is to display
        viewModel.getImageListMutableData().observe(this, Observer {
            adapter.setImageList(it)
            activityImageSearchBinding.gridView.invalidateViews()
        })

        // onclicklistener for image item from grid view
        activityImageSearchBinding.gridView.onItemClickListener =
            AdapterView.OnItemClickListener { _, _, position, _ ->
                val intent = Intent(this@ImageSearchActivity, ImageDetailsActivity::class.java)
                intent.putExtra(Constants.ARG_IMAGE, adapter.getItem(position))
                startActivity(intent)
            }

        // handles on search click on keyboard
        activityImageSearchBinding.etSearch.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                viewModel.getImages(activityImageSearchBinding.etSearch.text.toString())
                true
            } else {
                false
            }
        })

        // observes live data for empty list of images
        viewModel.getEmptyListMutableData().observe(this, Observer {
            when (it) {
                true -> activityImageSearchBinding.tvNoList.visibility = View.VISIBLE
                false -> activityImageSearchBinding.tvNoList.visibility = View.GONE
            }
        })

        // observes clear list of images
        viewModel.getClearListMutableData().observe(this, Observer {
            if (it == true) {
               adapter.clearListData()
            }
        })
    }

}