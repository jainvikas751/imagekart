package com.android.imagekart.android.data.remote

import com.android.imagekart.android.data.model.ImageResponse
import io.reactivex.Observable

import retrofit2.http.GET
import retrofit2.http.Query

interface ImageKartApi {

    @GET("gallery/search/1")
    fun getImages(
        @Query("q") query : String
    ): Observable<ImageResponse>
}